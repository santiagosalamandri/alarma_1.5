var searchData=
[
  ['secret',['secret',['../_init_task_8c.html#acf3e6b25218c40455fd7dbce5fd3ee82',1,'secret():&#160;InitTask.c'],['../main_8h.html#acf3e6b25218c40455fd7dbce5fd3ee82',1,'secret():&#160;InitTask.c']]],
  ['socket_5fqueue',['SOCKET_QUEUE',['../main_8h.html#aff1428d2393d77a998845908549342f7ab99bb307c819f8af1331e98b7c5d43ce',1,'main.h']]],
  ['socket_5fserver_2ec',['socket_server.c',['../socket__server_8c.html',1,'']]],
  ['socket_5fserver_2eh',['socket_server.h',['../socket__server_8h.html',1,'']]],
  ['socket_5ftask',['Socket_task',['../main_8h.html#a71c4bd219a8264d35620d3ae1029d1ff',1,'Socket_task(uint32_t):&#160;SocketTask.c'],['../_socket_task_8c.html#a4629c56370dbd043d93efae284d3421c',1,'Socket_task(uint32_t initial_data):&#160;SocketTask.c'],['../main_8h.html#a4e797bd98d8543afaea60b392543d68baa7b2f1f5199ad717c1f829a48faac2a3',1,'SOCKET_TASK():&#160;main.h']]],
  ['sockettask_2ec',['SocketTask.c',['../_socket_task_8c.html',1,'']]],
  ['sub_5fmenu_5fconfig',['sub_menu_config',['../config_8c.html#a966802129da385326806237141e85e6d',1,'sub_menu_config(char *option):&#160;config.c'],['../config_8h.html#a966802129da385326806237141e85e6d',1,'sub_menu_config(char *option):&#160;config.c']]],
  ['sub_5fmenu_5fstatus',['sub_menu_status',['../config_8c.html#a1bce600d28512a77546dea34f1f84a9d',1,'sub_menu_status(char *option):&#160;config.c'],['../config_8h.html#a1bce600d28512a77546dea34f1f84a9d',1,'sub_menu_status(char *option):&#160;config.c']]],
  ['sw_5fptr',['sw_ptr',['../_config_task_8c.html#ab4e3a0f750ff9fb308658e5bb8450867',1,'ConfigTask.c']]]
];
