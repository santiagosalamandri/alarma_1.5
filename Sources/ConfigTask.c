/*
 * ConfigTask.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */

#include "config.h"

void Config_task(uint32_t initial_data)
{
	_queue_id  socket_qid;				// Cola de mensaje del socket.
	APPLICATION_MESSAGE * 	msg;		// Puntero para referenciar el mensaje de la cola

	uint32_t	  			event_bits;	// Variable donde se almacena los bits de eventos
	LWTIMER_PERIOD_STRUCT	period;		// Estructura para el timer periodico
	LWTIMER_STRUCT			timer;		// Estructura para el timer
	uint8_t i;
	uint8_t tam_max=4;
	// printf("Ud. eligio configurar %d zonas\n",initial_data);

	if(NULL==(sw_ptr=malloc(tam_max*sizeof(LWGPIO_STRUCT))))
	{
	printf("fallo\n");
	exit -1;
	}

	printf("Config task started\n");
	//Se crea el evento para notificar a Socket Task


	int frecuency=(int)frec;
	printf("frecuencia: %d segundos\n",frecuency);
	printf("INICIANDO CONFIGURACION DE ZONAS \n");
	//_lwevent_create( &input_event, LWEVENT_AUTO_CLEAR);

	_int_disable();
	//_lwtimer_create_periodic_queue(&period, frecuency*BSP_ALARM_FREQUENCY, 0 );//Se crea la cola periodica de timers
	//_lwtimer_add_timer_to_queue( &period, &timer, 0, timer_isr, NULL );//Se agrega una timer a la cola
	//printf("termine los timers!!!\n\n");

	/*
	init_switch( sw_ptr, BSP_SW1, BSP_SW1_MUX_GPIO, port_ISR);
		init_switch( sw_ptr+1, BSP_SW2, BSP_SW2_MUX_GPIO, port_ISR);
		*/

	for(i=0;i<tam_max;i++)
		{
		init_switch(sw_ptr+i,ports[i],ports_mux[i],port_ISR);	//inicializando zonas
		//printf("inicializado pin %d\n",i);
		}


	 _int_enable();

	 printf("CONFIGURACION DE ZONAS TERMINADA\n");
//	 uint8_t event;
//	socket_qid = _msgq_get_id(0, SOCKET_QUEUE);		// Contiene el ID de la cola de mensajes enviados por socket

	_task_block();
}


