/*
 * ConfigTask.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */

#include "config.h"

void Config_task(uint32_t initial_data)
{
	_queue_id  socket_qid;				// Cola de mensaje del socket.
	APPLICATION_MESSAGE * 	msg;		// Puntero para referenciar el mensaje de la cola

	uint32_t	  			event_bits;	// Variable donde se almacena los bits de eventos
	LWTIMER_PERIOD_STRUCT	period;		// Estructura para el timer periodico
	LWTIMER_STRUCT			timer;		// Estructura para el timer
	uint8_t i;
	uint8_t tam_max=4;
	// printf("Ud. eligio configurar %d zonas\n",initial_data);

	if(NULL==(sw_ptr=malloc(tam_max*sizeof(LWGPIO_STRUCT))))
	{
	printf("fallo\n");
	exit -1;
	}

	printf("Config task started\n");
	//Se crea el evento para notificar a Socket Task


	int frecuency=(int)frec;
	printf("frecuencia: %d segundos\n",frecuency);
	printf("INICIANDO CONFIGURACION DE ZONAS \n");
	_lwevent_create( &input_event, LWEVENT_AUTO_CLEAR);

	_int_disable();
	_lwtimer_create_periodic_queue(&period, frecuency*BSP_ALARM_FREQUENCY, 0 );//Se crea la cola periodica de timers
	_lwtimer_add_timer_to_queue( &period, &timer, 0, timer_isr, NULL );//Se agrega una timer a la cola
	printf("termine los timers!!!\n\n");
	for(i=0;i<tam_max;i++)
		{
		init_switch(sw_ptr+i,ports[i],ports_mux[i],port_ISR);	//inicializando zonas
		printf("inicializado pin %d\n",i);
		}
	 _int_enable();

	 printf("CONFIGURACION DE ZONAS TERMINADA\n");
	 uint8_t event;
	socket_qid = _msgq_get_id(0, SOCKET_QUEUE);		// Contiene el ID de la cola de mensajes enviados por socket
	while (1) {
				_lwevent_wait_ticks( &input_event, 0xFFFF, FALSE, 0);
				event_bits = _lwevent_get_signalled();
				// _int_disable();
				 if((event_bits <= zones_quantity || event_bits == 20) && estado==ACTIVADO)
				 	 {
					 printf("\nEVENTO ENCONTRADO: %d <= cantidad de zonas %d\nSe envia notificacion\n",event_bits,zones_quantity);
					 alerta=ACTIVADO;
//					 _task_ready( _task_get_td(led_task_id) );

/*
					 if (led_task_id == MQX_NULL_TASK_ID) {
							 led_task_id=_task_create(0, LED_TASK, 0);
							 printf("Led_task_id: %d\n",led_task_id);
							 //_task_ready( _task_get_td(led_task_id) );
					 }
					 else
					 {
						 if(_task_restart(led_task_id,0,FALSE)==MQX_OK)
							 printf("reiniciado OK Led task\n");
						else
							 printf("Fallo reiniciado Led task\n");
					 }
*/
					 msg = _msg_alloc_system(sizeof(*msg));
						 if (msg != NULL) {
											//printf("\nEVENTO ENCONTRADO: %d\n",event_bits);
										 msg->HEADER.TARGET_QID = socket_qid;
										 msg->MESSAGE_TYPE = pow(2,event_bits);
										 msg->DATA = 0;
										 _msgq_send(msg);
										 }
					}
				 else
					 {
					printf("\nEVENTO ENCONTRADO: %d > cantidad de zonas %d\nNo se envia notificacion\n",event_bits,zones_quantity);
					 //printf(".\n");
					 }
				 _time_delay(1000);
				// _lwevent_clear(&input_event,event_bits);
				 //_int_enable();

			}
}


