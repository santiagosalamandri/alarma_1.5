/*
 * SocketTask.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */
#include "main.h"
#include <string.h>
#include <shell.h>

#ifndef LAN
void Socket_task(uint32_t initial_data)
{
	_queue_id  my_qid, socket_qid;
	APPLICATION_MESSAGE * msg;
	//char *data="algo,213124,01010100,123.4.23.123";
		char data[100]="";
		_ip_address server_ip;
		uint16_t		server_port;

		server_ip   = REMOTE_IP;

		server_port = 60000;
		initial_data= initial_data;
	_msgpool_create_system(sizeof(*msg), 10, 0, 0);

	my_qid = _msgq_open(SOCKET_QUEUE, 0);
	//socket_qid = _msgq_get_id(0, SOCKET_QUEUE);
	printf("Socket task started\n");

	while (1) {
		msg = _msgq_receive(my_qid, 0);


		printf("\nCliente socket recibi: %d\n",msg->MESSAGE_TYPE);
		//snprintf(data,(sizeof data),"%s,%s,%d,",user,pass,(int)msg->MESSAGE_TYPE);

		/*FORMATO DE MENSAJE
		 * id,name_user,is_alert,zone		 *
		 * */
		if(msg->MESSAGE_TYPE==0)
			snprintf(data,(sizeof data),"1,Santiago,%d,20",(int)msg->MESSAGE_TYPE); //mensaje de control
		else
			snprintf(data,(sizeof data),"1,Santiago,1,%d",(int)msg->MESSAGE_TYPE);
		_msg_free(msg);
		printf("el mensaje es: %s y tiene un tamanio de %d",data,strlen(data));
		IPCFG_IP_ADDRESS_DATA_PTR   data_ip;
			            struct addrinfo     addrinfo_hints;
			            struct addrinfo     *addrinfo_result;
			            struct addrinfo     *addrinfo_result_first;
			            int32_t             retval = 0;
			            char                addr_str[RTCS_IP4_ADDR_STR_SIZE];

			            _mem_zero(&addrinfo_hints, sizeof(addrinfo_hints));
			            //addrinfo_hints.ai_flags = AI_CANONNAME;
			            addrinfo_hints.ai_family 	= AF_UNSPEC;
			            addrinfo_hints.ai_socktype 	= SOCK_DGRAM;
			            _time_delay(2000);
			            retval = getaddrinfo("alarmserver.mooo.com", NULL, &addrinfo_hints, &addrinfo_result);
			         //   printf("retval %d\n",retval);
			            if (retval == 0) /* OK */
			            {
			                addrinfo_result_first = addrinfo_result;
			                /* Print all resolved IP addresses.*/
			               // while(addrinfo_result)
			                {//printf("IP: %s\n", addr_str);
			                    if(inet_ntop(addrinfo_result->ai_family,
			                                &((struct sockaddr_in6 *)((*addrinfo_result).ai_addr))->sin6_addr,
			                                addr_str, sizeof(addr_str)))
			                    {
			                        //printf("%s , %s\n", addrinfo_result->ai_canonname, addr_str);
			                    	printf("IP: %s\n", addr_str);
			                    }
			                    //addrinfo_result = addrinfo_result->ai_next;
			                }

			                //freeaddrinfo(addrinfo_result_first);

			               	//TCPClient(server_ip, server_port,data);

			                //ENVIANDO MENSAJE
			                TCPClient((addrinfo_result->ai_addr), server_port,data);
			            	////////////////////


			                //_time_delay(5 * 1000);
			            //		msg->HEADER.TARGET_QID = display_qid;
			            //		_msgq_send(msg);

			                	//printf("MENSAJE LIBERADO\n");
			            }
			            else
			            {
			                printf("Unable to resolve host\n");
			                //ENVIANDO MENSAJE
			               			                TCPClient((addrinfo_result->ai_addr), server_port,data);
			            }
			            _time_delay(500);
			            		lwgpio_int_enable(sw_ptr+msg->MESSAGE_TYPE, TRUE);

	 }
}

#endif
#ifdef LAN

void Socket_task(uint32_t initial_data)
{
	_queue_id  my_qid, socket_qid;
	APPLICATION_MESSAGE * msg;
	//char *data="algo,213124,01010100,123.4.23.123";
		char data[100]="";
		_ip_address server_ip;
		uint16_t		server_port;

		server_ip   = REMOTE_IP;

		server_port = 60000;
		initial_data= initial_data;
	_msgpool_create_system(sizeof(*msg), 10, 0, 0);

	my_qid = _msgq_open(SOCKET_QUEUE, 0);
	//socket_qid = _msgq_get_id(0, SOCKET_QUEUE);
	printf("Socket task started\n");
int last_int=-1;
int envio=0;
	while (1) {
		msg = _msgq_receive(my_qid, 0);
printf("llego un mensaje %d\n",msg->DATA);
		lwgpio_int_enable(sw_ptr+msg->DATA, TRUE);
		if(last_int!=msg->DATA & msg->DATA!=0)
				   {
						snprintf(data,(sizeof data),"1,Santiago,%d,%d",msg->MESSAGE_TYPE,msg->DATA+1); //mensaje de control
				   //if(last_int=!-1)lwgpio_int_enable(sw_ptr+last_int, TRUE);
				   last_int=msg->DATA;
				   envio=1;
				   }
		_msg_free(msg);
	   _time_delay(500);

	/*FORMATO DE MENSAJE
		 * id,name_user,is_alert,zone		 *
		 * */
	   if(envio)TCPClient(REMOTE_IP, server_port, data);
    	memset(data, 0, sizeof(data));
    	envio=0;

	 }
}

#endif

