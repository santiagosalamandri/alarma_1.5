/*
 * SocketThread.c
 *
 *  Created on: 13/08/2015
 *      Author: santi
 */

#include "sockets.h"

void TCPs_subthread(uint32_t temp)
{
    uint32_t     error;

    char        recv_buffer[TCPSERVER_RECV_BUF_SIZE + 1];
	char        send_buffer[TCPSERVER_SEND_BUF_SIZE];
	char *ok=strdup("OK");
	char *fallo=strdup("Fallo");
	char *alar_ac=strdup("Alarma Activada");
	char *alarm_desac=strdup("Alarma Desactivada");

	uint32_t     new_socket_handle  = temp;

	uint32_t     opt_val;
	uint32_t     opt_len;

	// set receive push to FALSE, recv will return only if buffer is full, and timeout if set.
	/*
	opt_val = FALSE;
	opt_len = sizeof(uint32_t);
	error = setsockopt(new_socket_handle, SOL_TCP, OPT_RECEIVE_PUSH, &opt_val, opt_len);
	if (error != RTCS_OK) {
		printf("\n[TCPServer]: Failed to setsockopt for OPT_RECEIVE_PUSH, CODE = 0x%x.", error);
		_task_block();
	}
*/
	// set receive timeout to 60s, recv will return received n bytes if timeout occur
	/*
	opt_val = 60 * 1000;
	opt_len = sizeof(uint32_t);
	error = setsockopt(new_socket_handle, SOL_TCP, OPT_RECEIVE_TIMEOUT, &opt_val, opt_len);
	if (error != RTCS_OK) {
		printf("\n[TCPServer]: Failed to setsockopt for OPT_RECEIVE_TIMEOUT, CODE = 0x%x.", error);
		_task_block();
	}
*/
	// clean buffer to ensure '\0' filled for recv and send.
	memset(recv_buffer, 0, sizeof(recv_buffer));
	memset(send_buffer, 0, sizeof(send_buffer));

	while (TRUE) {
		//if (read(new_socket_handle, recv_buffer, sizeof(recv_buffer)) == RTCS_ERROR) {

		if (recv(new_socket_handle, recv_buffer, TCPSERVER_RECV_BUF_SIZE, 0) == RTCS_ERROR) {
			error = RTCS_geterror(new_socket_handle);

			// Close TCPServer if TCPClient close first.
			if (error == RTCSERR_TCP_CONN_CLOSING) {
				shutdown(new_socket_handle, FLAG_ABORT_CONNECTION);
				printf("\n[TCPServer]: TCPClient Closed (Socket = 0x%x).", new_socket_handle);
				break;
			// Close TCPServer if TCPClient abort.
			} else if (error == RTCSERR_TCP_CONN_RESET) {
				shutdown(new_socket_handle, FLAG_ABORT_CONNECTION);
				printf("\n[TCPServer]: Connection reset by peer.");
				printf("\n[TCPServer]: TCPClient Closed (Socket = 0x%x).", new_socket_handle);
				break;
            // Timeout occur if zero char received.
			} else if (error == RTCSERR_TCP_TIMED_OUT) {
				printf("\n[TCPServer]: recv Timeout.");
				if (send(new_socket_handle, "TCPServer recv Timeout.", strlen("TCPServer recv Timeout.") + 1, 0) == RTCS_ERROR) {
					printf("\n[TCPServer]: Failed to send, CODE = 0x%x.", RTCS_geterror(new_socket_handle));
					_task_block();
				}
			} else {
				printf("\n[TCPServer]: Failed to recv, CODE = 0x%x.", error);
				_task_block();
			}
		} else {


			//strcpy(send_buffer, recv_buffer);
			//strcat(send_buffer, " echo by TCPServer.");
			printf("\n[TCPServer]: Se recibio el siguiente comando: %s", recv_buffer);
			memset(send_buffer, 0, sizeof(send_buffer));
			int val=authentication(recv_buffer);
			printf("valor de menu de salida: %d\n",val);
			switch(val)
			{
			case 0: strncpy(send_buffer,ok,strlen(ok));
					break;
			case 1:	strncpy(send_buffer,fallo,strlen(fallo));
					break;
			case 2:	strncpy(send_buffer,alar_ac,strlen(alar_ac));
					break;
			case 3:	strncpy(send_buffer,alarm_desac,strlen(alarm_desac));
					break;
			default:strncpy(send_buffer,fallo,strlen(fallo));
					break;
			}

			// append information to dentify a TIMEOUT return.
		/*
		 * if (strlen(recv_buffer) < TCPSERVER_RECV_BUF_SIZE) {
				printf(" recv() returned due to TIMEOUT.");
				strcat(send_buffer, " recv() returned due to TIMEOUT.");
			}
		 */
printf("enviando msj!!\n");
			// echo string back to TCPClient.
	if (send(new_socket_handle, send_buffer, strlen(send_buffer) + 1, 0) == RTCS_ERROR) {
//if (send(new_socket_handle, "OK-1", 4, 0) == RTCS_ERROR) {
				printf("\n[TCPServer]: Failed to send, CODE = 0x%x.", RTCS_geterror(new_socket_handle));
				_task_block();
			}
			printf("msj enviado!!\n");

			// clean buffer to ensure '\0' filled for next recv.
			memset(recv_buffer, 0, sizeof(recv_buffer));
		}
	}
}
