/*
 * LedTask.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */

#include "config.h"

void led_task(uint32_t initial_data)
{
	LWGPIO_STRUCT led1;

	if (!lwgpio_init(&led1, BSP_LED1, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE))
	    {
	        printf("Initializing LED1 GPIO as output failed.\n");
	        _task_block();
	    }
	    /* swich pin functionality (MUX) to GPIO mode */
	    lwgpio_set_functionality(&led1, BSP_LED1_MUX_GPIO);

	    /* write logical 1 to the pin */
	//    lwgpio_set_value(&led1, LWGPIO_VALUE_HIGH); /* set pin to 1 */


	 while (TRUE)  {
		 //if(alerta==ACTIVADO)
		 if(TRUE)
		 {
	        lwgpio_toggle_value(&led1);
	        _time_delay(250);
		 }
		 else
			 {
		lwgpio_set_value(&led1, LWGPIO_VALUE_HIGH); /* set pin to 1 */
			 //_task_block();

			 }
	    }
}


