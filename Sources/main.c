/****************************************************************************
* 
*   This file contains MQX only stationery code.
*
****************************************************************************/
#include "main.h"


#if !BSPCFG_ENABLE_IO_SUBSYSTEM
#error This application requires BSPCFG_ENABLE_IO_SUBSYSTEM defined non-zero in user_config.h. Please recompile BSP with this option.
#endif


#ifndef BSP_DEFAULT_IO_CHANNEL_DEFINED
#error This application requires BSP_DEFAULT_IO_CHANNEL to be not NULL. Please set corresponding BSPCFG_ENABLE_TTYx to non-zero in user_config.h and recompile BSP with this option.
#endif


TASK_TEMPLATE_STRUCT MQX_template_list[] = 
{ 
/*  Task number, 			Entry point, 	Stack, Pri, String, 			Auto? */
	{INIT_TASK,				Init_task,		1500,	11, "Init_task", 		MQX_AUTO_START_TASK},
	{TCPSERVER_TASK, 		TCPServer_task,	3000,	9,	"TCPServer",		0,	           		},
	{TCPS_SUBTHREAD,		TCPs_subthread, 2000,   9,	"TCPs_subthread",	0,			 		},
	//{SOCKET_TASK,			Socket_task,	1500,  	11, "Socket_task", 		0					},
	{SOCKET_TASK,			Socket_task,	2500,  	8, "Socket_task", 		0					},
	{CONFIG_TASK,   		Config_task,   	3000,	8,  "Config_task", 		0					},
	{LED_TASK,		        led_task, 		1500,	10,  "led",				MQX_AUTO_START_TASK },
	{OUT1_TASK,		        out1_task, 		1500,	9,  "out1",				0, 0,     0 },
	{OUT2_TASK,		        out2_task, 		1500,	9,  "out2",				0, 0,     0 },
	{OUT3_TASK,		        out3_task, 		1500,	9,  "out3",				0, 0,     0 },
	{OUT4_TASK,		        out4_task, 		1500,	9,  "out4",				0, 0,     0 },
	{0,           			0,           	0,     	0,   0,      			0,                 	}
};

/* EOF */
