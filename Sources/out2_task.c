/*
 * out2Taout2sk.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */

#include "config.h"

void out2_task(uint32_t initial_data)
{
	LWGPIO_STRUCT out2;

		//if (!lwgpio_init(&out2, BSP_ARDUINO_GPIO5, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE))
	if (!lwgpio_init(&out2, BSP_ARDUINO_GPIO5, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_LOW))
		    {
		        printf("Initializing GPIO 6 as output failed.\n");
		        _task_block();
		    }
		    /* swich pin functionality (MUX) to GPIO mode */
		    lwgpio_set_functionality(&out2, BSP_ARDUINO_GPIO6_MUX_GPIO);

		    /* write logical 1 to the pin */
		//    lwgpio_set_value(&out3, LWGPIO_VALUE_HIGH); /* set pin to 1 */


	 while (TRUE)  {
//	        lwgpio_toggle_value(&out2);
	        lwgpio_set_value(&out2,LWGPIO_VALUE_HIGH);
	        printf("||||\n");
	        _time_delay(1000);
	        lwgpio_set_value(&out2,LWGPIO_VALUE_LOW);
	        _time_delay(1000);
	        printf("....\n ");
	        _task_block();
		 }

}


