/*
 * out4Taout4sk.c
 *
 *  Created on: 23/02/2015
 *      Author: santi-deep64
 */

#include "config.h"

void out4_task(uint32_t initial_data)
{
	LWGPIO_STRUCT out4;

	//if (!lwgpio_init(&out4, BSP_ARDUINO_GPIO7, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE))
	if (!lwgpio_init(&out4, BSP_ARDUINO_GPIO7, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_LOW))

	    {
	        printf("Initializing GPIO 7 as output failed.\n");
	        _task_block();
	    }
	    /* swich pin functionality (MUX) to GPIO mode */
	    lwgpio_set_functionality(&out4, BSP_ARDUINO_GPIO7_MUX_GPIO);

	    /* write logical 1 to the pin */
	//    lwgpio_set_value(&out4, LWGPIO_VALUE_HIGH); /* set pin to 1 */


	 while (TRUE)  {
		 lwgpio_set_value(&out4,LWGPIO_VALUE_HIGH);
		 	        printf("\t\t||||\n ");
		 	        _time_delay(1500);
		 	        lwgpio_set_value(&out4,LWGPIO_VALUE_LOW);
		 	        _time_delay(1500);
		 	        printf("\t\t....\n ");
			        _task_block();

		 }

}


