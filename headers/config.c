
#include "config.h"

void timer_isr(void *param)
{
	APPLICATION_MESSAGE * msg;
	msg = _msg_alloc_system(sizeof(*msg));
			if (msg != NULL) {
				msg->HEADER.TARGET_QID =  _msgq_get_id(0, SOCKET_QUEUE);//socket_qid;
				msg->MESSAGE_TYPE = 0;
				msg->DATA = 0;
				_msgq_send(msg);
			}
}
/*
void port_ISR(void* param)
{
	LWGPIO_STRUCT_PTR sw = (LWGPIO_STRUCT_PTR) param;
	int i=0;
	while(lwgpio_int_get_flag(sw_ptr+i)==FALSE){i++;}
	lwgpio_int_clear_flag(sw_ptr+i);
	_lwevent_set(&input_event,i+1);
}
*/
void port_ISR(void* param)
{
	LWGPIO_STRUCT_PTR sw = (LWGPIO_STRUCT_PTR) param;
	APPLICATION_MESSAGE * msg;
	int i=0;
	while(lwgpio_int_get_flag(sw_ptr+i)==FALSE){i++;}

		lwgpio_int_clear_flag(sw_ptr+i);
		lwgpio_int_enable(sw_ptr+i, FALSE);
if(estado==ACTIVADO)
	{
		msg = _msg_alloc_system(sizeof(*msg));
		if (msg != NULL) {
			msg->HEADER.TARGET_QID =  _msgq_get_id(0, SOCKET_QUEUE);//socket_qid;
			 //msg->MESSAGE_TYPE = pow(2,i);
			msg->MESSAGE_TYPE = INT1;
			 msg->DATA = i+1;
			 _msgq_send(msg);
			}
	}

}


void init_switch( LWGPIO_STRUCT_PTR sw, LWGPIO_PIN_ID pin, uint32_t mux, INT_ISR_FPTR sw_isr)
{
	lwgpio_init(sw, pin, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE);

	lwgpio_set_functionality(sw, mux );

	lwgpio_set_attribute( sw, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);
	//lwgpio_set_attribute( sw, LWGPIO_ATTR_PULL_DOWN, LWGPIO_AVAL_ENABLE);
	//lwgpio_int_init( sw, LWGPIO_INT_MODE_FALLING);	//para interruptores
	lwgpio_int_init( sw, LWGPIO_INT_MODE_RISING);	//para sensores
	//lwgpio_int_init( sw, LWGPIO_INT_MODE_HIGH);	//para sensores
	_int_install_isr( lwgpio_int_get_vector(sw), sw_isr, &sw);

	_bsp_int_init(lwgpio_int_get_vector(sw), 3, 0, TRUE);
	lwgpio_int_enable(sw, TRUE);

}

