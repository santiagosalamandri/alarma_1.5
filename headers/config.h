/**
 * \file config.h
 *	\brief Contiene todas definiciones de las funciones necesarias para configurar las interrupciones externas y por timer.
 *  \date 13/08/2015
 *  \author Santiago Salamandri
 */

/*==================[macros and definitions]=================================*/
#ifndef CONFIG_H_
#define CONFIG_H_
/*==================[inclusions]=============================================*/
#include "main.h"

LWGPIO_STRUCT_PTR	sw_ptr;
LWEVENT_STRUCT  	input_event;
/*==================[external data definition]===============================*/
extern int frec;
extern int zones_quantity;
extern int config_task_id;
extern _task_id led_task_id;
extern _task_id out2_task_id;
extern _task_id out3_task_id;
extern _task_id out4_task_id;

extern int alerta;

/*==================[internal functions definition]==========================*/

/**
 * \fn void timer_isr(void *param)
 *
 * \brief Rutina de interrupcion de timer. Cada vez que es llamada, setea evento para notificar.
 *
 * \param param Puntero a dato opcional.
 *
 */
void timer_isr(void *param);

/**
 * \fn void port_ISR(void* param)
 * \brief Rutina de interrupcion de puertos externos. Cada vez que es llamada, setea evento segun el gpio que lo activo
 * \param param  puntero a dato opcional
 *
 */
void port_ISR(void* param);

/**
 * \fn void init_switch( LWGPIO_STRUCT_PTR sw, LWGPIO_PIN_ID pin, uint32_t mux, INT_ISR_FPTR sw_isr)
 * \brief Inicializa entradas para interrupciones externas
 *
 * \param sw Puntero a estructura de gpio a configurar
 * \param pin Posicion fisica donde se encuentra el gpio a configurar
 * \param mux Función de gpio a configurar
 * \param sw_isr Rutina de interrupción que atiende el gpio a configurar
 *
 */
void init_switch( LWGPIO_STRUCT_PTR sw, LWGPIO_PIN_ID pin, uint32_t mux, INT_ISR_FPTR sw_isr);


#endif /* CONFIG_H_ */
