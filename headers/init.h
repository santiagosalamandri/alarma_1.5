/*
 * 	\file init.h
 *	\brief Contiene todas las variables de estado del sistema.
 *  \date 13/08/2015
 *  \author Santiago SAlamandri
 */

#ifndef INIT_H_
#define INIT_H_

/*==================[inclusions]=============================================*/
#include "main.h"
/*==================[internal data definition]===============================*/
char *user="user";
//char *user="Santiago";
char *pass="user";
char *secret="secret";
int frec=60; 			///frecuencia inicial de envio periodico
int zones_quantity=0;
int config_task_id;
_task_id led_task_id;
_task_id out2_task_id;
_task_id out3_task_id;
_task_id out4_task_id;
int alerta= DESACTIVADO;
int out_2= DESACTIVADO;
int out_3= DESACTIVADO;
int out_4= DESACTIVADO;
#endif /* INIT_H_ */
