/**
* \file main.h
* \brief Contiene la tarea principal (main_task) del sistema.
* Incluye todas las librerías necesarias para correr la aplicación.
* Define las tareas que correrá el sistema.
* \authors Santiago Salamandri
*
*/

#ifndef __main_h_
#define __main_h_
/*==================[inclusions]=============================================*/
#include <string.h>
#include <stdlib.h>
#include <mqx.h>
#include <bsp.h>
#include <lwevent.h>
#include <lwtimer.h>
#include <message.h>
#include "menu.h"
#include "sockets.h"
#include <math.h>

#include <shell.h>
#include <rtcs.h>
#include <ipcfg.h>
#include <dns.h>
#include <dnscln.h>

#include "config.h"


#define ACTIVAR 1
#define DESACTIVAR 0
/*==================[external data definition]===============================*/
extern char *user;
extern char *pass;
extern char *secret;

/**
 * \enum ERROR_TEMPLATE
 * \brief Inicializa tareas de configuracion y servicios de red
 *
 */
typedef enum {
ERROR=1,
ERROR_AUTENTICACION,
ERROR_COMANDO,
ERROR_CONFIG,
ERROR_ESTADO,
ERROR_ZONAS
} ERROR_TEMPLATE;

/**
 * \enum TASK_TEMPLATE_INDEX_T
 * \brief Inicializa tareas de configuracion y servicios de red
 */
typedef enum {
	INIT_TASK=1,
	TCPSERVER_TASK,
	TCPS_SUBTHREAD,
	CONFIG_TASK,
	SOCKET_TASK,
	LED_TASK,
	OUT1_TASK,
	OUT2_TASK,
	OUT3_TASK,
	OUT4_TASK
} TASK_TEMPLATE_INDEX_T;

/**
 * \fn void Init_task(uint32_t)
 *
 * \brief Inicializa tareas de configuracion y servicios de red
 */
extern void Init_task(uint32_t initial_data);

/**
 * \fn void Config_task(uint32_t)
 *
 * \brief Tarea encargada de configurar las interrupciones y enviar las notificaciones a la tarea socket
 * \param
 */
extern void Config_task(uint32_t initial_data);

/**
 * \fn void Socket_task(uint32_t)
 *
 * \brief Tarea encargada de crear un cliente socket para comunicarse con el servidor web, en caso de alerta.
 *
 * \param initial_data Parámetro opcional.

 */
extern void Socket_task(uint32_t initial_data);

/**
 * \fn void Init_task(uint32_t)
 *
 * \brief Inicializa tareas de configuracion y servicios de red.
 *
 * \param initial_data Parámetro opcional.
 *
 */
extern void led_task(uint32_t initial_data);


/**
 * \fn void Init_task(uint32_t)
 *
 * \brief Inicializa tareas de configuracion y servicios de red.
 *
 * \param initial_data Parámetro opcional.
 */
extern void out1_task(uint32_t initial_data);

/**
 * \fn void Init_task(uint32_t)
 *
 * \brief Inicializa tareas de configuracion y servicios de red.
 *
 * \param initial_data Parámetro opcional.
 */
extern void out2_task(uint32_t initial_data);
/**
 * \fn void Init_task(uint32_t)
 * \brief Inicializa tareas de configuracion y servicios de red
 * \param initial_data Parámetro opcional.
 * \return
 */
extern void out3_task(uint32_t initial_data);
/**
 * \fn void Init_task(uint32_t)
 *
 * \brief Inicializa tareas de configuracion y servicios de red.
 *
 * \param initial_data Parámetro opcional.
 */
extern void out4_task(uint32_t initial_data);

int ports[]={
	BSP_ARDUINO_GPIO0 ,
	BSP_ARDUINO_GPIO1 ,
	BSP_ARDUINO_GPIO2 ,
	BSP_ARDUINO_GPIO3 ,
	BSP_ARDUINO_GPIO4 ,
	BSP_ARDUINO_GPIO5 ,
	BSP_ARDUINO_GPIO6 ,
	BSP_ARDUINO_GPIO7 ,
	BSP_ARDUINO_GPIO8 ,
	BSP_ARDUINO_GPIO9 ,
	BSP_ARDUINO_GPIO10 ,
	BSP_ARDUINO_GPIO11 ,
	BSP_ARDUINO_GPIO12 ,
	BSP_ARDUINO_GPIO13 ,
	BSP_ARDUINO_GPIO14 ,
	BSP_ARDUINO_GPIO15
   };

int ports_mux[]={
	BSP_ARDUINO_GPIO0_MUX_GPIO ,
	BSP_ARDUINO_GPIO1_MUX_GPIO ,
   	BSP_ARDUINO_GPIO2_MUX_GPIO ,
   	BSP_ARDUINO_GPIO3_MUX_GPIO ,
   	BSP_ARDUINO_GPIO4_MUX_GPIO ,
   	BSP_ARDUINO_GPIO5_MUX_GPIO ,
   	BSP_ARDUINO_GPIO6_MUX_GPIO ,
   	BSP_ARDUINO_GPIO7_MUX_GPIO ,
   	BSP_ARDUINO_GPIO8_MUX_GPIO ,
   	BSP_ARDUINO_GPIO9_MUX_GPIO ,
   	BSP_ARDUINO_GPIO10_MUX_GPIO ,
   	BSP_ARDUINO_GPIO11_MUX_GPIO ,
   	BSP_ARDUINO_GPIO12_MUX_GPIO ,
   	BSP_ARDUINO_GPIO13_MUX_GPIO ,
   	BSP_ARDUINO_GPIO14_MUX_GPIO ,
   	BSP_ARDUINO_GPIO15_MUX_GPIO
};

/**
 * \enum APPLICATION_QUEUE_T
 *
 * \brief Listado de interrupciones externas.
 *
 */
   	typedef enum {
	INT1=1,
	INT2,
	INT3,
	INT4,
	INT5,
	INT6,
	INT7,
	INT8
} APPLICATION_MESSAGE_TYPE_T;

/**
 * \struct APPLICATION_MESSAGE
 *
 * \brief Definicion de la estructura de los mensajes.
 *
 */
typedef struct {
	MESSAGE_HEADER_STRUCT 		HEADER;
	APPLICATION_MESSAGE_TYPE_T	MESSAGE_TYPE;
	uint32_t					DATA;
} APPLICATION_MESSAGE;

/**
 * \enum APPLICATION_QUEUE_T
 *
 * \brief Listado de colas de mensaje.
 *
 */
typedef enum {
	INTERRUPT_QUEUE,
	SOCKET_QUEUE
} APPLICATION_QUEUE_T;

#endif /* __main_h_ */
