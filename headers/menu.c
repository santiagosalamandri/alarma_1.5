/*
 * config.c
 *
 *  Created on: 20/05/2015
 *      Author: santi
 */

#include "menu.h"
#include "main.h"

int authentication(char* option)
{
//agregar "__STRICT_ANSI__=0"

	char *user_opt=strdup(option_parser(option,1));
	char *pass_opt=strdup(option_parser(option,2));

if((strcmp(user,user_opt)== 0) && (strcmp(pass,pass_opt)== 0)==1)
	{
	return menu_app(option);	//comprobar que termino bien
	}
else
	{
	printf("fallo autenticacion!!\nIngrese nuevamente\n");
	return 1;
	}
}


char* option_parser(char *options,int index)
{
char *option, *string;
int i=0;

//strcpy(string,options);
string=strdup(options);
while(i<index)
           {
		option = strsep(&string, ";");
		i++;
		}
return option;
}


int menu_app(char * option)
{
	char *ptr;
	int op=strtol(option_parser(option,3),&ptr,0);

	printf("Ud. esta en el menu principal y eligio la opcion %s -",option_parser(option,3));
switch(op)
			{
			case 1 :	printf("MENU DE CONFIGURACION\n");
						sub_menu_config(option);
						break;
			case 2 :	printf("MENU DE ESTADO\n");
						return sub_menu_status(option);
						break;
			case 3 :	printf("MENU DE ACTIVACION\n");
						activate(option);
						//sub_menu_activate(option);
						break;
			case 4 :	printf("MENU DE ACCION\n");
									action(option);
									//sub_menu_activate(option);
									break;
			default :	return 1;
						break;

			}
return 0;	//termino ok
}

int sub_menu_config(char * option)
{
	char *ptr;
		int op=strtol(option_parser(option,4),&ptr,0);
	switch(op)
				{
				case 1 :	printf("CONFIGURACION DE ZONAS\n");
							config_zones(option);
							break;
				case 2 :	printf("CAMBIO DE PASSWORD\n");
							change_password(option);
							break;
				case 3 :  	printf("CAMBIO DE FRECUENCIA\n");
							change_frecuency(option);
							break;
				default :	return 1;
							break;

				}
	return 0;	//termino ok

	}

int sub_menu_status(char * option)
{
	 printf("El estado de la alarma es: ");
	 if(estado==1)
		 {
		 printf("ACTIVADA");
		 return 2; //codigo alarma activada
		 }
	 else printf("DESACTIVADA");

		return 3;	//codigo alarma desactivada

	//show status
	}

int config_zones(char *option)
{
	 printf("Ud. eligio configurar %s zonas\n",option_parser(option,5));
	 //ENVIAR CANTIDAD DE ZONAS A CONFIG TASK
	 int opt = *option_parser(option,5) - '0';//transforma de char a integer
	 //_task_create(0, CONFIG_TASK, opt);
	 zones_quantity=opt;
	 if (config_task_id == MQX_NULL_TASK_ID) {
	 /* Create the task: */
		 config_task_id=_task_create(0, CONFIG_TASK, opt);
		 printf("config_task_id: %d\n",config_task_id);
	 }
	 else
	 {

		 printf("tarea ya creada, se cambia el numero de zonas\n");
		   //_task_abort(config_task_id);
		 // _task_destroy(config_task_id);
	 	 //config_task_id=_task_create(0, CONFIG_TASK, opt);

		 //_task_restart(config_task_id,&(opt),FALSE);
	 }
	 return 0;	//termino ok

}
int change_password(char *option)
{
	pass=strdup(option_parser(option,5));
	printf("Su nuevo password es: %s \n",option_parser(option,5));
	return 0;	//termino ok

}
int change_frecuency(char *option)
{
	printf("La nueva frecuencia de actualizacion es %s\n",option_parser(option,5));
	int opt = *option_parser(option,5) - '0';
	frec=opt;
	//ENVIAR FRECUENCIA DE ACTUALIZACION A CONFIG TASK
	return 0;	//termino ok

}
int activate(char *option)
{
	int opt = *option_parser(option,4) - '0';
	//HABILITAR INTERRUPCIONES
	estado=opt;

	if(estado==ACTIVADO)
		{
		//comprobar entradas de la alarma antes de activar
		printf("ALARMA ACTIVADA y alerta %d\n",alerta);
		}
	//DESHABILITAR INTERRUPCIONES
	else if(estado==DESACTIVADO)
	{
		printf("ALARMA DESACTIVADA\n");
		alerta=DESACTIVADO;
	}
		else return 1;

	return 0;	//termino ok
}

int action(char *option)
{
	int opt = *option_parser(option,4) - '0';
	//HABILITAR INTERRUPCIONES

switch(opt)
{
case 1:
		_task_restart(out2_task_id,0,FALSE);
		break;
case 2:
		_task_restart(out2_task_id,0,FALSE);
		break;
case 3:
		_task_restart(out3_task_id,0,FALSE);
		break;
case 4:
		_task_restart(out4_task_id,0,FALSE);
		break;
default:
		printf("Opción no válida\n");
		return 1;
}
	return 0;	//termino ok
}


int encrypt(char* text,char* key, char* encr, int size)
{
int i;
int j=strlen(key);

for(i=0;i<size;i++)
	{
	encr[i]=text[i]^key[i%j];
	}
return 0;
}

int decrypt(char* encr,char* key, char* decr, int size)
{
int i;
int j=strlen(key);
for(i=0;i<size;i++)
	{
	decr[i]=encr[i]^key[i%j];
	}
return 0;
}
