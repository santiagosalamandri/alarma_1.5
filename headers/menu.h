/**
* \file menu.h
* \brief Contiene las definiciones de las funciones del menú de opciones.
*  Estas funciones son de parseo, autenticación, submenú, configuración de alarma.
*/

#ifndef MENU_H_
/**
 * \def menu.h
 * \brief Se definen parametros de red, estados de alarma y funciones de configuracion
 * \param
 * */
#define MENU_H_


#define ACTIVADO 1
#define DESACTIVADO 0
#define ALERTA DESACTIVADO

int estado = DESACTIVADO; /// Estado inicial de la alarma.
char *user; /// Puntero para editar nombre de usuario.
char *pass; /// Puntero para editar password de usuario.
int frec; /// Variable que indica la frecuencia de actualización periódica de la alarma.
int zones_quantity; /// Cantidad de zonas de la alarma.
int config_task_id; /// ID de la tarea config.
char key[] = "password"; /// Clave inicial para encriptar.

/*!
 * \fn int authentication(char* option)
 * \brief Funcion que verifica usuario y contraseña
 * \param option Puntero con string que se parsea para verificar usuario y contraseña
 * \return 0=OK 1=Error
 */
int authentication(char* option);

/**
 * \fn char* option_parser(char *options,int index)
 * \brief Funcion que parsea el parametro de entrada
 * \param option Puntero con string que se parsea
 * \param index Entero que indica que la posicion del parámetro a devolver
 * \return Puntero a parámetro seleccionado
 */
char* option_parser(char *options,int index);

/**
 * \fn int menu_app(char * option)
 * \brief Determina la función seleccionada
 * \param option Puntero con string que se parsea para seleccionar la función
 * \return 0=OK 1=Error
 */
int menu_app(char * option);

/**
 * \fn int sub_menu_config(char * option)
 * \brief Determina la función de configuración seleccionada
 * \param option Puntero con string que se parsea para seleccionar la función de configuración
 * \return 0=OK 1=Error
 */
int sub_menu_config(char * option);

/**
 * \fn int sub_menu_status(char * option)
 * \brief Determina el estado de la alarma
 * \param option Puntero con string que se parsea para seleccionar la función de Estado
 * \return 0=OK 1=Error
 */
int sub_menu_status(char * option);

/**
 * \fn int activate(char *option)
 * \brief Activa o desactiva la alarma
 * \param option Puntero con string que se parsea para seleccionar la función de Activación
 * \return 0=OK 1=Error
 */
int activate(char *option);
/**
 * \fn int config_zones(char *option)
 * \brief Configura la cantidad de zonas
 * \param option Puntero con string que se parsea para seleccionar la cantidad de zonas de la alarma
 * \return 0=OK 1=Error
 */
int config_zones(char *option);


int action(char *option);


/**
 * \fn int change_password(char *option)
 * \brief Modifica el password de la alarma
 * \param option Puntero con string que se parsea para cambiar el password de la alarma
 * \return 0=OK 1=Error
 */
int change_password(char *option);

/**
 * \fn int change_frecuency(char *option)
 * \brief Cambia la frecuencia de actualización periódica de la alarma
 * \param option Puntero con string que se parsea para seleccionar la Frecuencia de actualización de la alarma
 * \return 0=OK 1=Error
 */
int change_frecuency(char *option);

int algorithm(void);

int encrypt(char* text,char* key, char* encr, int size);
int decrypt(char* enc,char* key, char* decr, int size);

#endif
/* EOF */
