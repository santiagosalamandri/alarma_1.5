/*
 * socket.c
 *
 *  Created on: 12/08/2015
 *      Author: santi
 */

#include "sockets.h"


void ip_to_digital(_ip_address ip, _ip_digital *p)
{
	p->high       = (ip >> 24) & 0xFF;
	p->middlehigh = (ip >> 16) & 0xFF;
	p->middlelow  = (ip >> 8)  & 0xFF;
	p->low        = (ip)       & 0xFF;
}


void TCPServer(uint16_t port)
{
	uint32_t     error;

    sockaddr    server_socket_addr;
    sockaddr    client_socket_addr;
	uint16_t     socket_addr_len = sizeof(client_socket_addr);

    uint32_t     socket_handle;
	uint32_t     new_socket_handle;

	_ip_digital digit;

	printf("\n[TCPServer]: Launching TCPServer...");
	//printf("\n[TCPServer]: Due to the limited RAM resources in MCU, don't open more than 3 TCPclients at the same time.");

	// Create a stream socket for TCPServer
    socket_handle = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_handle == RTCS_SOCKET_ERROR) {
        printf("\n[TCPServer]: Failed to create socket.");
        _task_block();
    }

	// bind socket to server, use INADDR_ANY to specify any IP address
    server_socket_addr.sin_family      = AF_INET;
    server_socket_addr.sin_port        = port;
    server_socket_addr.sin_addr.s_addr = INADDR_ANY;
	error = bind(socket_handle, &server_socket_addr, sizeof(server_socket_addr));
	if (error != RTCS_OK) {
		printf("\n[TCPServer]: Failed to bind socket, CODE = 0x%x.", error);
		_task_block();
	}

	// put the stream socket into listening state to allow incoming connection requests.
	error = listen(socket_handle, 0);
	if (error != RTCS_OK) {
		printf("\n[TCPServer]: Failed to listen socket, CODE = 0x%x.", error);
		_task_block();
	}

	while (TRUE)
	{
		// block task until incoming connection accpeted, create a new stream socket
		new_socket_handle = accept(socket_handle, &client_socket_addr, &socket_addr_len);
		if (new_socket_handle != RTCS_SOCKET_ERROR) {
			ip_to_digital(client_socket_addr.sin_addr.s_addr, &digit);
			printf("\n[TCPServer]: Accepted connection from IP = %d.%d.%d.%d, Port = %d.",
				   digit.high, digit.middlehigh, digit.middlelow, digit.low, client_socket_addr.sin_port);

			// create sub thread to service client's request.
			_task_create(0, TCPS_SUBTHREAD, new_socket_handle);
		} else {
			error = RTCS_geterror(socket_handle);
			// ? When will accept return RTCS_SOCKET_ERROR & RTCS_OK ?
			if (error == RTCS_OK) {
                shutdown(socket_handle, FLAG_ABORT_CONNECTION);
				printf("\n[TCPServer]: Connection reset by peer.");
                printf("\n[TCPServer]: Closed.");
                break;
			} else {
				printf("\n[TCPServer]: Failed to accept, CODE = 0x%x", error);
				_task_block();
			}
		}
	}
}


#ifdef LAN
void TCPClient(_ip_address ip, uint16_t port, char *msg)
{
	uint32_t     error;								/** */
    int intentos=0;

    char        buffer[TCPCLIENT_RECV_BUF_SIZE];	/** */

    sockaddr    server_socket_addr;					/** */

    uint32_t     socket_handle;						/** */

	_ip_digital digit;								/** */

	uint32_t     opt_val;
	uint32_t     opt_len;

	printf("\n[TCPClient]: Launching TCPClient...");

	opt_val = 3 * 60 * 1000;						/** */
	opt_len = sizeof(uint32_t);						/** */

	// set remote host(TCPServer) socket address
    server_socket_addr.sin_family      = AF_INET;
    server_socket_addr.sin_port        = port;
    server_socket_addr.sin_addr.s_addr = ip;

	// loop to establish connection
    do {
    	printf("Intento numero %d.\n",intentos);

		// Create a stream socket for TCPClient.
		socket_handle = socket(AF_INET, SOCK_STREAM, 0);
		if (socket_handle == RTCS_SOCKET_ERROR) {
			printf("\n[TCPClient]: Failed to create socket.");
			//_task_block();
			_task_restart(_task_get_id(),0,TRUE);

		}

		// set 3mins for connection Timeout
		error = setsockopt(socket_handle, SOL_TCP, OPT_CONNECT_TIMEOUT, &opt_val, opt_len);
		if (error != RTCS_OK) {
			printf("\n[TCPClient]: Failed to setsockopt for OPT_CONNECT_TIMEOUT, CODE = 0x%x.", error);
			_task_block();
		}

		// try to connect TCPServer
		error = connect(socket_handle, &server_socket_addr, sizeof(server_socket_addr));
		if (error != RTCS_OK) {
			if (error == RTCSERR_TCP_TIMED_OUT) {
				printf("\n[TCPClient]: Connection timeout.");
			} else if (error == RTCSERR_TCP_CONN_RESET) {
				printf("\n[TCPClient]: Connection reset by peer."); // When connect to PC but TCPServer shuntdown, will alert this error
			}
		    shutdown(socket_handle, FLAG_ABORT_CONNECTION);
			printf("\n[TCPClient]: Retry to connect TCPServer 5 seconds later...");
			_time_delay(5 * 1000);
			intentos++;
		}
    } while (error != RTCS_OK && intentos<=3);

   	// display connection message
       if(intentos<3)
       {

	// display connection message
	ip_to_digital(ip, &digit);
    printf("\n[TCPClient]: Connected to TCPServer, IP = %d.%d.%d.%d, Port = %d.",
		   digit.high, digit.middlehigh, digit.middlelow, digit.low, port);

 	// clean buffer to ensure '\0' filled for recv
	memset(buffer, 0, sizeof(buffer));

    while (TRUE) {
		// Send msg to TCPServer
    	printf("\nenviando mensaje!\n");
    	memset(buffer, 0, sizeof(buffer));

    	snprintf(msg,100,"%s,%d.%d.%d.%d",msg,digit.high, digit.middlehigh, digit.middlelow, digit.low);
    	//char buffer[]="algo,213124,01010100,123.4.23.123";
		printf("el mensaje a enviar es: \n %s \n y el tamanio es: %d o %d",msg,sizeof(msg),strlen(msg));

		if (send(socket_handle,msg, strlen(msg) + 1,0) == RTCS_ERROR) {
					printf("\n[TCPClient]: Failed to send, CODE = 0x%x.", RTCS_geterror(socket_handle));
					_task_block();
		}
    	printf("mensaje enviado!\n");
    	memset(buffer, 0, sizeof(buffer));


		// wait to receive msg from TCPServer
        if (recv(socket_handle, buffer, sizeof(buffer), 0) == RTCS_ERROR) {
			error = RTCS_geterror(socket_handle);

				// Close TCPClient if TCPServer close first.
				if (error == RTCSERR_TCP_CONN_CLOSING) {
					shutdown(socket_handle, FLAG_ABORT_CONNECTION);
					printf("\n[TCPClient]: Closed.");
					break;
				// Close TCPClient if TCPServer abort.
				} else if (error == RTCSERR_TCP_CONN_RESET) {
					shutdown(socket_handle, FLAG_ABORT_CONNECTION);
					printf("\n[TCPClient]: Connection reset by peer.");
					printf("\n[TCPClient]: Closed.");
					break;
				} else {
					printf("\n[TCPClient]: Failed to recv, CODE = 0x%x.", error);
					_task_block();
				}
        }
        else {
				printf("\nSERVIDOR: \"%s\".", buffer);

				// clean buffer to ensure '\0' filled for next recv.
				memset(buffer, 0, sizeof(buffer));

				shutdown(socket_handle, FLAG_ABORT_CONNECTION);
				break;
			}

    }
}
  else
  {
	printf("Se supero la cantidad de intentos de envio\n");
  }
}
#endif
#ifndef LAN
void TCPClient(sockaddr* ip, uint16_t port, char *msg)
{
	uint32_t     error;								/** */

    char        buffer[TCPCLIENT_RECV_BUF_SIZE];	/** */

    sockaddr    server_socket_addr;					/** */

    uint32_t     socket_handle;						/** */

	_ip_digital digit;								/** */

	uint32_t     opt_val;
	uint32_t     opt_len;

	printf("\n[TCPClient]: Launching TCPClient...");

	opt_val = 3 * 60 * 1000;						/** */
	opt_len = sizeof(uint32_t);						/** */

	// set remote host(TCPServer) socket address
    server_socket_addr.sin_family      = AF_INET;
    server_socket_addr.sin_port        = port;
    //server_socket_addr.sin_addr.s_addr = ip;
    //server_socket_addr.sin_addr.s_addr = ENET_IPGATEWAY;
    if(LAN)
    	server_socket_addr.sin_addr.s_addr = REMOTE_IP;
    else
    server_socket_addr.sin_addr.s_addr = ip->sin_addr.s_addr;

    int intentos=0;
int a=0;
	// loop to establish connection
    do {
		// Create a stream socket for TCPClient.
    	printf("Intento numero %d.\n",intentos);
		socket_handle = socket(AF_INET, SOCK_STREAM, 0);
		if (socket_handle == RTCS_SOCKET_ERROR) {
			printf("\n[TCPClient]: Failed to create socket.");
			//_task_block();
			_task_restart(_task_get_id(),0,TRUE);

		}
		// set 3mins for connection Timeout
		error = setsockopt(socket_handle, SOL_TCP, OPT_CONNECT_TIMEOUT, &opt_val, opt_len);
		if (error != RTCS_OK) {
			printf("\n[TCPClient]: Failed to setsockopt for OPT_CONNECT_TIMEOUT, CODE = 0x%x.", error);
			_task_block();
		}
		// try to connect TCPServer
		error = connect(socket_handle, &server_socket_addr, sizeof(server_socket_addr));
		printf("\nERROR: %d\n",error);
		if (error != RTCS_OK) {
			if (error == RTCSERR_TCP_TIMED_OUT) {
				printf("\n[TCPClient]: Connection timeout.");
			} else if (error == RTCSERR_TCP_CONN_RESET) {
				printf("\n[TCPClient]: Connection reset by peer."); // When connect to PC but TCPServer shuntdown, will alert this error
			}
		    shutdown(socket_handle, FLAG_ABORT_CONNECTION);
			printf("\n[TCPClient]: Retry to connect TCPServer 5 seconds later...");
			_time_delay(5 * 1000);
			intentos++;

		}
    } while (error != RTCS_OK && intentos<=3);

	// display connection message
    if(intentos<3)
    {
    	ip_to_digital((_ip_address)ip->sin_addr.s_addr, &digit);
    printf("\n[TCPClient]: Connected to TCPServer, IP = %d.%d.%d.%d, Port = %d.",
		   digit.high, digit.middlehigh, digit.middlelow, digit.low, port);

 	// clean buffer to ensure '\0' filled for recv
	memset(buffer, 0, sizeof(buffer));

    while (TRUE) {
		// Send msg to TCPServer
    	printf("\nenviando mensaje!\n");
    	memset(buffer, 0, sizeof(buffer));

    	snprintf(msg,100,"%s,%d.%d.%d.%d",msg,digit.high, digit.middlehigh, digit.middlelow, digit.low);
    	//char buffer[]="algo,213124,01010100,123.4.23.123";
		printf("el mensaje a enviar es: \n %s \n y el tamanio es: %d o %d",msg,sizeof(msg),strlen(msg));

		if (send(socket_handle,msg, strlen(msg) + 1,0) == RTCS_ERROR) {
					printf("\n[TCPClient]: Failed to send, CODE = 0x%x.", RTCS_geterror(socket_handle));
					_task_block();
		}
    	printf("mensaje enviado!\n");
    	memset(buffer, 0, sizeof(buffer));


		// wait to receive msg from TCPServer
        if (recv(socket_handle, buffer, sizeof(buffer), 0) == RTCS_ERROR) {
			error = RTCS_geterror(socket_handle);

				// Close TCPClient if TCPServer close first.
				if (error == RTCSERR_TCP_CONN_CLOSING) {
					shutdown(socket_handle, FLAG_ABORT_CONNECTION);
					printf("\n[TCPClient]: Closed.");
					break;
				// Close TCPClient if TCPServer abort.
				} else if (error == RTCSERR_TCP_CONN_RESET) {
					shutdown(socket_handle, FLAG_ABORT_CONNECTION);
					printf("\n[TCPClient]: Connection reset by peer.");
					printf("\n[TCPClient]: Closed.");
					break;
				} else {
					printf("\n[TCPClient]: Failed to recv, CODE = 0x%x.", error);
					_task_block();
				}
        }
        else {
				printf("\nSERVIDOR: \"%s\".", buffer);

				// clean buffer to ensure '\0' filled for next recv.
				memset(buffer, 0, sizeof(buffer));

				shutdown(socket_handle, FLAG_ABORT_CONNECTION);
				break;
			}

    }
    }
    else
    {
    	printf("Se supero la cantidad de intentos de envio\n");
    }
}
#endif


uint32_t setup_network(void)
{
ip_data.ip = ENET_IPADDR;
ip_data.mask = ENET_IPMASK;
ip_data.gateway = ENET_IPGATEWAY;

error = RTCS_create();
if (error) return error;
#ifdef DEBUG
printf("OK\n");
#endif
ENET_get_mac_address (BSP_DEFAULT_ENET_DEVICE, ENET_IPADDR, enet_address);
error = ENET_initialize(BSP_DEFAULT_ENET_DEVICE, enet_address, 0, &ehandle);
if (error) return error;
#ifdef DEBUG
printf("OK\n");
#endif
error = RTCS_if_add(ehandle, RTCS_IF_ENET, &ihandle);
if (error) return error;
#ifdef DEBUG
printf("OK\n");
#endif
error = ipcfg_init_interface(BSP_DEFAULT_ENET_DEVICE, ihandle);
if (error) return error;
#ifdef DEBUG
printf("OK\n");
#endif
error= ipcfg_bind_autoip(BSP_DEFAULT_ENET_DEVICE, &ip_data);
//error= ipcfg_bind_dhcp_wait(BSP_DEFAULT_ENET_DEVICE, TRUE, &ip_data);
if (error) return error;
#ifdef DEBUG
printf("OK\n");
#endif
ipcfg_get_ip(BSP_DEFAULT_ENET_DEVICE,&ip_data);

	printf("IP: %d.%d.%d.%d\n", IPBYTES(ip_data.ip));
	printf("Mask: %d.%d.%d.%d\n", IPBYTES(ip_data.mask));
	printf("gateway: %d.%d.%d.%d\n", IPBYTES(ip_data.gateway));

#ifdef DEBUG
	printf("DNS: %d.%d.%d.%d\n", IPBYTES(ipcfg_get_dns_ip(BSP_DEFAULT_ENET_DEVICE,0)));
#endif

//printf("Añadiendo DNS\n");
if(ipcfg_add_dns_ip(BSP_DEFAULT_ENET_DEVICE,ENET_IPGATEWAY))
		printf("dns actualizado!!\n");

printf("DNS actualizado: %d.%d.%d.%d\n", IPBYTES(ipcfg_get_dns_ip(BSP_DEFAULT_ENET_DEVICE,0)));

return(0);
}

