/*
 * \file socket.h
 *	\brief Contiene las definiciones de las funciones necesarias para la comunicación de red.

 *  \date 05/03/2015
 *  \author: Santiago Salamandri
 */
#ifndef SOCKETS_H_

#define SOCKETS_H_
/*==================[inclusions]=============================================*/
#include "main.h"
#include <enet.h>
#include <ipcfg.h>
#include "menu.h"

/*==================[macros and definitions]=================================*/
#define LAN 1

#ifndef ENET_IPADDR
	//#define ENET_IPADDR    IPADDR(192, 168, 88, 202)
	//#define ENET_IPADDR    IPADDR(192, 168, 1, 202)
#define ENET_IPADDR    IPADDR(192, 168, 0, 202)
//#define ENET_IPADDR    IPADDR(172,18,189,105)
//#define ENET_IPADDR    IPADDR(10,42,0,2)
#endif

#ifndef ENET_IPMASK
	#define ENET_IPMASK    IPADDR(255, 255, 255, 0)
//#define ENET_IPMASK    IPADDR(255, 255, 252, 0)
#endif

#ifndef ENET_IPGATEWAY
	//#define ENET_IPGATEWAY IPADDR(0, 0, 0, 0)
	//#define ENET_IPGATEWAY IPADDR(192,168,0,1)
#define ENET_IPGATEWAY IPADDR(192,168,0,253)
	//#define ENET_IPGATEWAY IPADDR(172,18,188,1)
//#define ENET_IPGATEWAY IPADDR(10,42,0,1)
#endif

//#define REMOTE_IP	   IPADDR(192, 168, 88, 201)
//#define REMOTE_IP	   IPADDR(192, 168, 1, 201)
#define REMOTE_IP	   IPADDR(192, 168, 0, 201)	///Direccion IP del servidor remoto

#define ENABLE_TCP_SERVER
#define ENABLE_TCP_CLIENT

#define TCPSERVER_RECV_BUF_SIZE     50 /// Buffer de recepción del servidor socket
#define TCPSERVER_SEND_BUF_SIZE	    50 /// Buffer de envío del socket server
//#define TCPS_SUBTHREAD 30

#define TCPCLIENT_RECV_BUF_SIZE     100 /// Buffer de recepción del cliente socket
#define TCPCLIENT_TASK_INDX         4 ///

//#define ENET_IPADDR IPADDR(192,168,0,202)
//#define ENET_IPMASK IPADDR(255,255,255,0)
#define ENET_IPGATEWAY IPADDR(192,168,0,253)
	//#define ENET_IPGATEWAY IPADDR(172,18,188,1)
//#define ENET_IPGATEWAY IPADDR(10,42,0,1)

//#define GATE_GOOGLE IPADDR(8,8,8,8)

IPCFG_IP_ADDRESS_DATA ip_data;
_enet_address enet_address;
_enet_handle ehandle;
_rtcs_if_handle ihandle;
uint32_t error;

/**
 * \struct _ip_digital
 *
 * \brief Estructura que define los octetos de una direccion IP
 */
typedef struct
{
	uint8_t high;
	uint8_t middlehigh;
	uint8_t middlelow;
	uint8_t low;
} _ip_digital;

/**
 * \fn void TCPServer(uint16_t port)
 *
 * \brief Crea un servidor socket en la dirección ip "ENET_IPADDR" y puerto "port"
 *
 * \param uint16_t port Puerto donde escucha el servidor
 */
void TCPServer(uint16_t port);

/**
 * \fn void TCPServer_task(uint32_t temp)
 *
 * \brief
 *
 * \param uint32_t temp  Parámetro opcional.
 */
void TCPServer_task(uint32_t temp);

/**
 * \fn void TCPs_subthread(uint32_t temp)
 * \brief Función que crea un hilo por cada conexión entrante al servidor.
 *
 * Se limita a 3 conexiones por cuestiones de uso de memoria.
 *
 * \param uint32_t temp Parámetro opcional.
 */
void TCPs_subthread(uint32_t temp);

/**
 * \fn void TCPClient(_ip_address ip, uint16_t port,char *msg)
 * \brief Función que crea un cliente Socket para establecer una comunicación con un servidor socket en la IP y puerto pasado como parámetros.
 *
 * \param _ip_address ip Dirección IP del servidor socket con el que se desea comunicar.
 * \param uint16_t port Puerto al que se desea conectar.
 */

//void TCPClient(_ip_address ip, uint16_t port,char *msg);
#ifdef LAN
void TCPClient(_ip_address ip, uint16_t port, char *msg);
#endif
#ifndef LAN
void TCPClient(sockaddr* ip, uint16_t port,char *msg);

#endif

/**
 * \fn void ip_to_digital(_ip_address ip, _ip_digital *p)
 */
void ip_to_digital(_ip_address ip, _ip_digital *p);
/**
 * \fn void ip_to_digital(_ip_address ip, _ip_digital *p)
 */
uint32_t setup_network(void);


#endif /* SOCKET_H_ */
