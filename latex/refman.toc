\select@language {spanish}
\contentsline {chapter}{\numberline {1}\IeC {\'I}ndice de estructura de datos}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Estructura de datos}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Indice de archivos}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Lista de archivos}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Documentaci\IeC {\'o}n de las estructuras de datos}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Referencia de la Estructura \discretionary {-}{}{}\_\discretionary {-}{}{}ip\discretionary {-}{}{}\_\discretionary {-}{}{}digital}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Descripci\IeC {\'o}n detallada}{5}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Referencia de la Estructura A\discretionary {-}{}{}P\discretionary {-}{}{}P\discretionary {-}{}{}L\discretionary {-}{}{}I\discretionary {-}{}{}C\discretionary {-}{}{}A\discretionary {-}{}{}T\discretionary {-}{}{}I\discretionary {-}{}{}O\discretionary {-}{}{}N\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}M\discretionary {-}{}{}E\discretionary {-}{}{}S\discretionary {-}{}{}S\discretionary {-}{}{}A\discretionary {-}{}{}G\discretionary {-}{}{}E}{5}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Descripci\IeC {\'o}n detallada}{5}{subsection.3.2.1}
\contentsline {chapter}{\numberline {4}Documentaci\IeC {\'o}n de archivos}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Referencia del Archivo headers/config.h}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Descripci\IeC {\'o}n detallada}{7}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Documentaci\IeC {\'o}n de las funciones}{8}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}init\discretionary {-}{}{}\_\discretionary {-}{}{}switch}{8}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}port\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}I\discretionary {-}{}{}S\discretionary {-}{}{}R}{8}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}timer\discretionary {-}{}{}\_\discretionary {-}{}{}isr}{8}{subsubsection.4.1.2.3}
\contentsline {subsection}{\numberline {4.1.3}Documentaci\IeC {\'o}n de las variables}{8}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}zones\discretionary {-}{}{}\_\discretionary {-}{}{}quantity}{8}{subsubsection.4.1.3.1}
\contentsline {section}{\numberline {4.2}Referencia del Archivo headers/main.h}{8}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Descripci\IeC {\'o}n detallada}{10}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Documentaci\IeC {\'o}n de las enumeraciones}{10}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}A\discretionary {-}{}{}P\discretionary {-}{}{}P\discretionary {-}{}{}L\discretionary {-}{}{}I\discretionary {-}{}{}C\discretionary {-}{}{}A\discretionary {-}{}{}T\discretionary {-}{}{}I\discretionary {-}{}{}O\discretionary {-}{}{}N\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Q\discretionary {-}{}{}U\discretionary {-}{}{}E\discretionary {-}{}{}U\discretionary {-}{}{}E\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}T}{10}{subsubsection.4.2.2.1}
\contentsline {subsection}{\numberline {4.2.3}Documentaci\IeC {\'o}n de las funciones}{10}{subsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.3.1}Config\discretionary {-}{}{}\_\discretionary {-}{}{}task}{10}{subsubsection.4.2.3.1}
\contentsline {subsubsection}{\numberline {4.2.3.2}Init\discretionary {-}{}{}\_\discretionary {-}{}{}task}{10}{subsubsection.4.2.3.2}
\contentsline {subsubsection}{\numberline {4.2.3.3}Socket\discretionary {-}{}{}\_\discretionary {-}{}{}task}{11}{subsubsection.4.2.3.3}
\contentsline {subsection}{\numberline {4.2.4}Documentaci\IeC {\'o}n de las variables}{11}{subsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.4.1}ports}{11}{subsubsection.4.2.4.1}
\contentsline {subsubsection}{\numberline {4.2.4.2}ports\discretionary {-}{}{}\_\discretionary {-}{}{}mux}{11}{subsubsection.4.2.4.2}
\contentsline {section}{\numberline {4.3}Referencia del Archivo headers/menu.h}{11}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Descripci\IeC {\'o}n detallada}{12}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Documentaci\IeC {\'o}n de las funciones}{12}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}activate}{12}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}authentication}{13}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}change\discretionary {-}{}{}\_\discretionary {-}{}{}frecuency}{13}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}change\discretionary {-}{}{}\_\discretionary {-}{}{}password}{13}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}menu\discretionary {-}{}{}\_\discretionary {-}{}{}app}{13}{subsubsection.4.3.2.5}
\contentsline {subsubsection}{\numberline {4.3.2.6}option\discretionary {-}{}{}\_\discretionary {-}{}{}parser}{14}{subsubsection.4.3.2.6}
\contentsline {subsubsection}{\numberline {4.3.2.7}sub\discretionary {-}{}{}\_\discretionary {-}{}{}menu\discretionary {-}{}{}\_\discretionary {-}{}{}config}{14}{subsubsection.4.3.2.7}
\contentsline {subsubsection}{\numberline {4.3.2.8}sub\discretionary {-}{}{}\_\discretionary {-}{}{}menu\discretionary {-}{}{}\_\discretionary {-}{}{}status}{14}{subsubsection.4.3.2.8}
\contentsline {subsection}{\numberline {4.3.3}Documentaci\IeC {\'o}n de las variables}{14}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}zones\discretionary {-}{}{}\_\discretionary {-}{}{}quantity}{14}{subsubsection.4.3.3.1}
\contentsline {chapter}{\IeC {\'I}ndice}{15}{section*.14}
